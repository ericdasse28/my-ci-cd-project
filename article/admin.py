from django.contrib import admin
from article.models import Article, Category, Comment


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'visibility')
    list_editable = ('visibility',)
    list_filter = ('created_at', 'category')
    list_per_page = 2
    fields = [('id', 'title', 'slug', 'cover_image'),
              ('publish_time', 'visibility'),
              'content', 'category', 'summary', 'image_preview']
    autocomplete_fields = ('category',)
    prepopulated_fields = {"slug": ("title",)}
    readonly_fields = ("id", "image_preview")
    search_fields = ('title',)

    def image_preview(self, obj):
        return obj.image_preview

    image_preview.short_description = 'Cover image'
    image_preview.allow_tags = True


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_filter = ('name',)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('author_name', 'content', 'created_at')
    list_display_links = ('content',)
    list_filter = ('created_at',)
    autocomplete_fields = ('article',)
    search_fields = ('author_name',)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Comment, CommentAdmin)
