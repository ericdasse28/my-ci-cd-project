from rest_framework import serializers
from .models import Article, Category, Comment


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    comments = serializers.SlugRelatedField(many=True, read_only=True, slug_field='author_name')

    class Meta:
        model = Article
        fields = ('title', 'summary', 'content', 'category', 'comments')


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'author_name', 'content', 'created_at', 'article')


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')
