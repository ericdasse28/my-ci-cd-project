from django.db import models
from django.urls import reverse

from django.utils.safestring import mark_safe


class Category(models.Model):
    """Cet objet représente une catégorie ou un genre littéraire"""

    name = models.CharField(max_length=200, help_text='Enter an article category (ex: Actualité)')
    image_url = models.ImageField(upload_to='category_images/', help_text="uploadez votre image",
                                  verbose_name="Image", null=True)

    class Meta:
        verbose_name_plural = "Categories"
        ordering = ['name']

    def __str__(self):
        """Cette fonction est obligatoirement requise par Django.
        Elle retourne une chaine de caractères pour identifier l'instance de la classe d'objet."""

        return self.name


class Article(models.Model):
    title = models.CharField(max_length=200, help_text="Enter a title for the article", verbose_name="titre")
    summary = models.TextField(max_length=2000, help_text='Enter a brief description of the article')
    content = models.TextField(help_text='Enter the article content')
    slug = models.SlugField(max_length=200, help_text="Enter the article url", null=True, blank=True, default="")
    cover_image = models.ImageField(upload_to='image', help_text='Enter an image url for the article', null=True)

    thumbnail_image_url = models.CharField(max_length=2048, help_text='Enter a thumbnail url for the article',
                                           null=True, default='')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    publish_time = models.DateTimeField(null=True, help_text='Publication date of the article')
    visibility = models.BooleanField(default=False, help_text="Visibilité de l'article")
    file = models.FileField(upload_to='files/', help_text="Ajouter un fichier à l'article", null=True, blank=True)

    category = models.ForeignKey(Category, help_text='Select a category for this article', on_delete=models.SET_NULL,
                                 null=True)

    @property
    def image_preview(self):
        if self.cover_image:
            return mark_safe(f'<img src="{self.cover_image.url}" width="300" height="200" />')
        return ""

    def __str__(self):
        """Fonction requise par Django pour manipuler les objets Book dans la base de données"""
        return self.title

    # def get_absolute_url(self):
    #     """Cette fonction est requise par Django, lorsque vous souhaitez détailler le contenu d'un objet."""
    #     return reverse('article-detail', args=[str(self.id)])


class Comment(models.Model):
    author_name = models.CharField(max_length=200, help_text="Enter the article author", blank=True)
    content = models.TextField(max_length=2000, help_text="Enter the comment content")
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    article = models.ForeignKey(Article, help_text="Choose your article", on_delete=models.SET_NULL,
                                null=True, default='', blank=True, related_name='comments')

    def __str__(self):
        return self.content
